close all
clear all
clc

%% Load Estimation solution

An = csvread('An.csv');
Bn = csvread('Bn.csv');

ps = [0;0;0];
zshat = [0;0;1];
N = 4;

disp(['An = ', num2str(An)])
disp(['Bn = ', num2str(Bn)])


%% Symbolic Dynamic Functions Generator

[JBwState,JBwPew,JBwGew,JBwPtw,Bw,Be] = symbolicDynamicsGenerator(An,Bn,N);



