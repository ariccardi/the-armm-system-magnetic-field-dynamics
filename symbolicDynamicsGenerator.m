function [JBwState,JBwPew,JBwGew,JBwPtw,Bw,Be] = symbolicDynamicsGenerator(An,Bn,N)

%% Robot DH Table

syms q1 q2 q3 q4 q5 q6 real

q = [q1; q2; q3; q4; q5; q6];

ed = 0.055 + 0.05;

UR10DHTable = [pi/2        0           0.1273            q1;
               0          -0.612       0                 q2;
               0          -0.5723      0                 q3;
               pi/2        0           0.163941          q4;
              -pi/2        0           0.1157            q5;
               0           0           0.0922 + ed       q6];

           
%% UR10 Kinematics
         
NumberOfJoints = size(UR10DHTable,1);

A = cell(NumberOfJoints,1);

for i = 1:NumberOfJoints
    
    alpha = UR10DHTable(i,1);
    a = UR10DHTable(i,2);
    d = UR10DHTable(i,3);
    theta = UR10DHTable(i,4);
    
    DHTransformation = [ cos(theta) -sin(theta)*cos(alpha)  sin(theta)*sin(alpha) a*cos(theta);
                         sin(theta)  cos(theta)*cos(alpha) -cos(theta)*sin(alpha) a*sin(theta);
                         0           sin(alpha)             cos(alpha)            d;
                         0           0                      0                     1];
 
    A{i} = DHTransformation;
end

% Direct kinematics

T = eye(4);
T0i = cell(NumberOfJoints,1);

for i = 1:NumberOfJoints
    T = T*A{i};
    T = simplify(T);
    T0i{i} = T;
end

% Field Coordinate Transformation matrix

T0E = T0i{6};


%% UR10 Differential Kinematics

% Analytic jacobian
P0E = T0E(1:3,4);
G0E = [atan2(T0E(3,2),T0E(3,3));atan2(-T0E(3,1),sqrt( ((T0E(3,2))^2) + ((T0E(3,3))^2) ));atan2(T0E(2,1),T0E(1,1))];

JAnalitical = jacobian([P0E;G0E],q);
JAnalitical = simplify(JAnalitical);

JL = JAnalitical(1:3,:);
JA = JAnalitical(4:6,:);


%% Symbolic Coefficents for field model  

syms Ie gte zze yze xze zse yse xse zte yte xte real
syms ztw ytw xtw ptw zew yew xew pew real


%% Coordinate Transformation

% Roll Pitch Yaw Rotation Matrices
syms psie thee phie real
gew = [psie;thee;phie];

Rx = [1               0               0;
      0               cos(psie)       -sin(psie);
      0               sin(psie)        cos(psie)];
  
Ry = [cos(thee)        0               sin(thee);
      0                1               0;
     -sin(thee)        0               cos(thee)];

Rz = [cos(phie)       -sin(phie)        0;
      sin(phie)        cos(phie)        0;
      0                0               1];
  
Rew = Rz*Ry*Rx;

ptw = [xtw;ytw;ztw];
pew = [xew;yew;zew];

pte = (Rew')*(ptw - pew);


%% Vectors Definition in Electromagnet reference Frame

pee = [0;0;0];
zhe = [0;0;1];

rte = pte - pee;

rte_norm = sqrt(rte(1)^2 + rte(2)^2 + rte(3)^2);

rht = rte/rte_norm;


%% Legendre Polynomials

syms gtw real

for n = 1:1:N
    Ple(n) = legendreP(n,gtw);
    Ple_dot(n) = diff(Ple(n),gtw);
end

gtw = subs(gtw,zhe(1)*rht(1) + zhe(2)*rht(2) + zhe(3)*rht(3));

Ple = eval(Ple);
Ple_dot = eval(Ple_dot);


%% Field Coefficients 

% An part can be neglected since An(n) = 0 for all n
% for n = 1:1:N
%     C1(n) = An(n)*n*(rte_norm^(n - 1)) - ((n + 1)*Bn(n))/(rte_norm^(n + 2));
%     C2(n) = An(n)*(rte_norm^(n - 1)) + Bn(n)/(rte_norm^(n + 2));
% end


for n = 1:N
    C1(n) = - ((n + 1)*Bn(n))/(rte_norm^(n + 2));
    C2(n) = + Bn(n)/(rte_norm^(n + 2));
end

%% Field Model in Electromagnet Reference Frame

% Unit Current Model
Bue = [0;0;0];

for n = 1:N
    Bue = Bue + (C2(n)*gtw*Ple_dot(n) - C1(n)*Ple(n))*rht - C2(n)*Ple_dot(n)*zhe;
end

% Bue = vpa(Bue,5)

% Model in Electromagnet Reference Frame
Be = Bue*Ie;
% Be = vpa(Be,5)

%% Field Model in World Reference Frame

% BwU = Rew*Bue;
Bw = Rew*Be;


%% Model Jacobians World Reference Frame

% JBwUPew = jacobian(BwU, pew);
JBwPew = jacobian(Bw, pew);
JBwPtw = jacobian(Bw, ptw);
% JBwUGew = jacobian(BwU, gew);
JBwGew = jacobian(Bw, gew);
JBwIe  = jacobian(Bw, Ie);

% JBwU = JBwUPew*JL + JBwUGew*JA;
JBwState = [(JBwPew*JL + JBwGew*JA) JBwIe];


