# The ARMM System - Magnetic Field Dynamics

## Introduction
The ARMM System [1] is a mobile electromagnetic coil mounted on a 6DOFs serial manipulator. In this repository, the Matlabb code for building the symbolic exspression of its magnetic field dynamics is provided. The magnetic field model is built using a spherical multipole expansion [2], for which numerical coeffients are obtained through identification [3]. <br />
<br />
To cite this repository use:<br /><br />
@online{field-dynamics,<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;author={Riccardi, A. and Furtado, G. P. and Sikorski, J. and Vendittelli, M. and Misra, S.},<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;title = {The {ARMM System} - Magnetic Field Dynamics},<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;url  = {https://gitlab.tudelft.nl/ariccardi/the-armm-system-magnetic-field-dynamics.git}<br />
}<br />
<br />
A. Riccardi, G. P. Furtado, J. Sikorski, M. Vendittelli, and S. Misra. The ARMM system - Magnetic field dynamics. [Online]. Available: https://gitlab.tudelft.nl/ariccardi/the-armm-system-magneticfield-dynamics.git
## Documentation
-- Run main.m to execute the code. It will return the symbolic exspressions of:<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. JBwState: matrix B(theta(t)) representing the input transition matrix of the state dynamics.<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. Bw: magnetic field expressed in world reference frame.<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. Be: magnetic field expressed in the electromagnet reference frame.<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4. JBwPew: Jacobian of the field Bw w.r.t. the position of the electromagnet Pew in world reference frame.<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5. JBwGew: Jacobian of the field Bw w.r.t. the roll-pitch-yaw angles in world reference frame.<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6. JBwPtw: Jacobian of the field Bw w.r.t. the position of the target Ptw in world reference frame.<br />
<br />
-- Additionally, the repository includes:<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. Bn coefficients obtained from the field identification. They are used to define the model of the electromagnet. (An coefficients are zero because they represent the field inside the coil)<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. symbolicDynamicsGenerator.m is the code producing the symbolic expressions listed in main.m

## References
[1] J. Sikorski, C. M. Heunis, F. Franco, and S. Misra, “The ARMM System: An Optimized Mobile Electromagnetic Coil for Non-Linear Actuation of Flexible Surgical Instruments,” in IEEE Transactions on Magnetics, vol. 55, pp. 1-9, 2019.<br />
[2] J. D. Jackson, Classical Electrodynamics. American Association of Physics Teachers, 1999.<br />
[3] A. J. Petruska, J. Edelmann, and B. J. Nelson, “Model-Based Calibration for Magnetic Manipulation,” IEEE Transactions on Magnetics, vol. 53, p. 6, 2017.<br />
